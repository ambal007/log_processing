#!/bin/env python
#/home/nflcwebdev/log_processing/apache_access.py

bashCommand = "scp -i /home/nflcwebdev/.ssh/tvstapps3.pem vagrant@tvstapps3:/var/log/apache2/access.log /home/nflcwebdev/log_processing/access.log"
import subprocess
process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
output = process.communicate()[0]

http_err = [100,101,200,201,202,203,204,205,206,300,301,303,304,305,400,401,402,403,404,405,406,407,408,409,410,411,412,413,414,500,501,502,503,504,505 ]
http_err_str=[]
for item in http_err:
  http_err_str.append(str(item))

import re , sys
regex = '([(\d\.)]+) - - \[(.*?)\] \"(.*?)\" (\d+) (\d+) \"(.*?)\" \"(.*?)\"' 

aList = []
itemN = ""
item0 = item01 = item4 = ""
i1 = i2 = 0

for line in open('/home/nflcwebdev/log_processing/proxi_access.log'):
#for line in open('/home/vagrant/access.log'):
  if ( re.match(regex, line) ) is None or len(line) == 0:

#vvvvvvvvvvvvvvvvvvv NO REG EX vvvvvvvvvvvvvvvvvvvvvvvvv
    line = line.split('-')
    item0 = line [ 0 ].split(' ')
    item01 = item0[0].split(',')
    ip_address = str(item01)[2:len(item01)-3]
    for i in range( len(line) ):
      if re.search("\:\d+\:\d+\:\d+",line[ i ]):
        i1 = i
        break

    yyyymdhms = str(line[ i ])[2:]
    yyyymdhms = re.sub("/","\t" ,yyyymdhms);
    yyyymdhms = re.sub(":","\t" ,yyyymdhms);
    yyyymdhms = yyyymdhms.split("\t")

    if len(yyyymdhms) == 6:
      yyyymdhms = yyyymdhms[2]+'\t'+\
      yyyymdhms[1]+'\t'+\
      yyyymdhms[0]+'\t'+\
      yyyymdhms[len(yyyymdhms)-3]+'\t'+\
      yyyymdhms[len(yyyymdhms)-2]+'\t'+ \
      yyyymdhms[len(yyyymdhms)-1]
    else:
      yyyymdhms = ""
      continue

    itemN = itemN + yyyymdhms + '\t'

    for i in range(i1, len(line) ):
      if re.search("GET",line[ i ]) or re.search("POST",line[ i ] ):
        i2 = i
        break
    item4 = line[ i2 ].split(' ')
    getpost = url = err = ""
    for j in range( len(item4) ):
      i=item4[j].find("GET")
      ii=item4[j].find("POST")
      if i!=-1 :
        getpost = str(item4[ j ])[i:]
        break
      if ii!=-1 :
        getpost = str(item4[ j ])[ii:]
        break
    item5=line[ len(line) -1 ].split(' ')
    j=0
    for item in item5:
      i = item5[j].find("http")
      if i !=-1:
        url = item5[j][i:-1]
        break
      j = j + 1
    item4 = item4 + item5

    for j in range( len(item4) ):
      if (item4[j]) in http_err_str :
        if re.search(r"\d{3}",item4[j]):
          err = item4[j]
          break

    itemN = itemN + getpost+'\t'+err+'\t'+ip_address+'\t'+url

    #itemN = itemN + '\t' + ' NO REG EX' #test
    #print itemN

    aList.append( itemN )

    itemN = ""
#^^^^^^^^^^^^^^^^^^^^ NO REG EX ^^^^^^^^^^^^^^^^^^^^^^^^^^
    continue

##################### YES REG EX ##########################
  line = list ( re.match(regex, line).groups() )

  ip_address = line [ 0 ]
  yyyymdhms = line [ 1 ]
  m1=re.search(r"\d+\/[A-Z][a-z]+\/\d{4}",yyyymdhms)
  if m1:
    yyyymdhms = str(yyyymdhms)[0:]
    yyyymdhms = re.sub("/","\t" ,yyyymdhms);
    yyyymdhms = re.sub(":","\t" ,yyyymdhms);
    yyyymdhms = yyyymdhms.split("\t")
    item5 = yyyymdhms[5].split('-')

    yyyymdhms[5] = item5[0]
    yyyymdhms = yyyymdhms[2]+"\t"+yyyymdhms[1]+"\t"+yyyymdhms[0]+"\t"+ \
    yyyymdhms[3]+"\t"+yyyymdhms[4]+"\t"+yyyymdhms[5]+"\t"

  itemN = itemN + yyyymdhms

  getpost = line [ 2 ]
  item2 = getpost.split(' ')
  getpost = item2[ 0 ]
  itemN = itemN + getpost
  itemN = itemN + '\t'

  error = line[ 3 ]
  if int(error) in http_err :
    itemN = itemN + error
    itemN = itemN + '\t'

  itemN = itemN + ip_address
  itemN = itemN + '\t'

  url = line[len(line) - 2]+line[len(line) - 1]
  url1 = ""
  if re.search(' ',url):
    url = url.split(' ')
    for j in range( len(url) ):
      i = url[j].find("http")
      if i!=-1 :
        url1 = url[j][i:-1]
  url = url1
  itemN = itemN + url

  #itemN = itemN + '\t' + ' YES REG EX' #test

  #print itemN

  aList.append( itemN )
  itemN = ""

##################### YES REG EX ##########################

#for line in open('/home/nflcwebdev/log_processing/proxi_access.log'):

aList = list( set( aList ) )

#for line in aList:
#  print line

########################## ZIP FILE ##########################

import time
t=time.localtime()
mon = str(t.tm_mon)
if t.tm_mon < 10:
  mon = '0' + mon
day = str(t.tm_mday)
if t.tm_mday < 10:
  day = '0' + day
hour = str(t.tm_hour)
if t.tm_hour < 10:
  hour = '0' + hour
min1 = str(t.tm_min)
if t.tm_min < 10:
  min1 = '0' + min1
sec1 = str(t.tm_sec)
if t.tm_sec < 10:
  sec1 = '0' + sec1

time1 = str(t.tm_year)+' '+mon+' '+day+' '+hour+' '+min1+' '+sec1

fname1 = '/home/nflcwebdev/log_processing/' + str(t.tm_year)+mon+day + '.log'
print fname1
print time1

import os

os.system('ls -l ' + fname1)
itemList = aList
if not os.path.exists( fname1 ):
  print fname1 + " NOT EXIST\n"
  outfile = open( fname1, 'w')
  for line in itemList:
    if line[-1:] != '\n':
       line = line + '\n'
    outfile.write( line )
  outfile.close()
else:
  print fname1 + " IS HERE\n"
  infile = open( fname1, 'r')
  for line in infile:
    line = line.strip(' \n\r')
    itemList.append( line )

  itemList = list( set( itemList ) )

  outfile = open( fname1, 'w')
  for line in itemList:
    if line[-1:] != '\n':
       line = line + '\n'
    outfile.write( line )
  outfile.close()

# 55 * * * * /usr/bin/python /home/nflcwebdev/log_processing/apache_access.py >>/home/nflcwebdev/log_processing/cron_apache_access.log 2>&1
if not (t.tm_hour == 23 and t.tm_min == 55) :
  sys.exit('NOT ZIP ' + fname1 +' YET, EXIT')
##################################### SQLITE ###########################

import sqlite3
if not os.path.exists( '/home/nflcwebdev/log_processing/test_access.db' ):
  os.system('touch /home/nflcwebdev/log_processing/test_access.db/')
  conn = sqlite3.connect('/home/nflcwebdev/log_processing/test_access.db')
  c = conn.cursor()
  c.execute('CREATE TABLE apache_access_log(YEAR INT NOT NULL,MONTH TEXT NOT NULL,DAY INT NOT NULL,HOUR INT NOT NULL,MINUTE INT NOT NULL,SECOND INT NOT NULL,HTTP_METHOD CHAR(10),HTTP_ERR INT,IP_ADDRESS CHAR(30),URL VARCHAR(255))')
  conn.commit()
else:
  conn = sqlite3.connect('/home/nflcwebdev/log_processing/test_access.db')
  c = conn.cursor()
  c.execute('DROP TABLE apache_access_log') 
  c.execute('CREATE TABLE apache_access_log(YEAR INT NOT NULL,MONTH TEXT NOT NULL,DAY INT NOT NULL,HOUR INT NOT NULL,MINUTE INT NOT NULL,SECOND INT NOT NULL,HTTP_METHOD CHAR(10),HTTP_ERR INT,IP_ADDRESS CHAR(30),URL VARCHAR(255))')

print len(itemList)
for line in itemList:
  line = line.split('\t')
  c.execute('INSERT INTO apache_access_log VALUES(?,?,?,?,?,?,?,?,?,?)',line)

conn.commit()
for row in c.execute('SELECT * FROM apache_access_log;'):
  print row
for row in c.execute('SELECT count(*) FROM apache_access_log;'):
  print row

print len(itemList)

conn.close()

os.system( 'ls -l ' + fname1 )
os.system( 'gzip -f ' + fname1 )
os.system( 'ls -l ' + fname1 + '.gz' )
os.system( 'rm -f ' + fname1 )

print fname1
print time1
os.system( 'date' )

