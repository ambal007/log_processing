#!/bin/env python
#/home/nflcwebdev/log_processing/apache_error.py

bashCommand = "scp -i /home/nflcwebdev/.ssh/tvstapps3.pem vagrant@tvstapps3:/var/log/apache2/error.log /home/nflcwebdev/log_processing/error.log"
import subprocess
process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
output = process.communicate()[0]

import re, sys

aList = []
itemN = ""
#for line in open('/home/nflcwebdev/log_processing/error.log'):
for line in open('/home/nflcwebdev/log_processing/proxi_error.log'):
  line = line.split( '] [' )
  if len(line) <= 1:
    continue
  m = re.search("err" , line[ 1 ])
  if not m :
    continue

  item0 = line[0].split(' ')
  yyyymdhms = item0[len(item0)-1]
  yyyymdhms = yyyymdhms + '\t' + item0[ 1 ]+'\t'+item0[ 2 ]+'\t'
  item01 = item0[ 3 ].split(':')
  yyyymdhms = yyyymdhms + item01[0] + '\t' + item01[1]
  item02 = item01[len(item01) - 1].split('.')
  yyyymdhms = yyyymdhms + '\t' + item02[ 0 ]

  line[ len(line)-1 ] = re.sub("\]","" , line[ len(line)-1 ]);
  itemN = yyyymdhms + '\t' + str(line[ len(line)-1 ])[0:-1]

  aList.append( itemN )
  itemN = ""

aList = list ( set ( aList ) )

#for line in aList:
#  print line

########################## ZIP FILE ##########################

import time
t=time.localtime()
mon = str(t.tm_mon)
if t.tm_mon < 10:
  mon = '0' + mon
day = str(t.tm_mday)
if t.tm_mday < 10:
  day = '0' + day
hour = str(t.tm_hour)
if t.tm_hour < 10:
  hour = '0' + hour
min1 = str(t.tm_min)
if t.tm_min < 10:
  min1 = '0' + min1
sec1 = str(t.tm_sec)
if t.tm_sec < 10:
  sec1 = '0' + sec1

time1 = str(t.tm_year)+' '+mon+' '+day+' '+hour+' '+min1+' '+sec1

fname1 = '/home/nflcwebdev/log_processing/' + str(t.tm_year)+mon+day + '_err.log'
print fname1
print time1

import os

os.system('ls -l ' + fname1)
itemList = aList
if not os.path.exists( fname1 ):
  print fname1 + " NOT EXIST\n"
  outfile = open( fname1, 'w')
  for line in itemList:
    if line[-1] != '\n':
       line = line + '\n'
    outfile.write( line )
  outfile.close()
else:
  print fname1 + " IS HERE\n"
  infile = open( fname1, 'r')
  for line in infile:
    line = line.strip(' \n\r')
    itemList.append( line )

  itemList = list( set( itemList ) )

  outfile = open( fname1, 'w')
  for line in itemList:
    if line[-1] != '\n':
       line = line + '\n'
    outfile.write( line )
  outfile.close()

# 57 * * * * /usr/bin/python /home/nflcwebdev/log_processing/apache_error.py >>/home/nflcwebdev/log_processing/cron_apache_error.log 2>&1
if not (t.tm_hour == 23 and t.tm_min == 57) :
  sys.exit('NOT ZIP ' + fname1 +' YET, EXIT')
##################################### SQLITE ###########################

import sqlite3
if not os.path.exists( '/home/nflcwebdev/log_processing/test_error.db' ):
  os.system('touch /home/nflcwebdev/log_processing/test_error.db/')
  conn = sqlite3.connect('/home/nflcwebdev/log_processing/test_error.db')
  c = conn.cursor()
  c.execute('CREATE TABLE apache_error_log(YEAR INT NOT NULL,MONTH TEXT NOT NULL,DAY INT NOT NULL,HOUR INT NOT NULL,MINUTE INT NOT NULL,SECOND INT NOT NULL,ERROR VARCHAR(255))')
  conn.commit()
else:
  conn = sqlite3.connect('/home/nflcwebdev/log_processing/test_error.db')
  c = conn.cursor()
  c.execute('DROP TABLE apache_error_log')
  c.execute('CREATE TABLE apache_error_log(YEAR INT NOT NULL,MONTH TEXT NOT NULL,DAY INT NOT NULL,HOUR INT NOT NULL,MINUTE INT NOT NULL,SECOND INT NOT NULL,ERROR VARCHAR(255))')

for line in itemList:
  line = line.split('\t')
  c.execute('INSERT INTO apache_error_log VALUES(?,?,?,?,?,?,?)',line)

conn.commit()
for row in c.execute('SELECT * FROM apache_error_log;'):
  print row
for row in c.execute('SELECT count(*) FROM apache_error_log;'):
  print row
print len(itemList)
conn.close()

os.system( 'ls -l ' + fname1 )
os.system( 'gzip -f ' + fname1 )
os.system( 'ls -l ' + fname1 + '.gz' )
os.system( 'rm -f ' + fname1 )

print fname1
print time1
os.system( 'date' )

