#!/bin/env python
#/home/nflcwebdev/log_processing/mysql_error.py

#bashCommand = "scp -i /home/nflcwebdev/.ssh/tvstapps3.pem vagrant@tvstapps3:/var/log/mysql/mysql.error.log /home/nflcwebdev/log_processing/mysql.error.log"
#import subprocess
#process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
#output = process.communicate()[0]

import re , sys
regex = '^(\d+)\s+(\d\d:\d\d:\d\d)\s+(.*?)$'
aList = []
aList1 = []
itemN = ""
for line in open('/home/nflcwebdev/log_processing/mysql.error.log'):
  if not re.search("\d+",line):
    line = line.strip(' \t\n\r')
    if len(line) == 0 :
      continue
    aList1.append( line )
    continue
  if re.match(regex, line) is None or len(line) == 0:
    continue

  line = list ( re.match(regex, line).groups() )
  yr = line[0][:2]
  mo = line[0][2:4]
  dd = line[0][-2:]
  itemN = itemN + yr+ '\t' + mo + '\t' + dd
  itemN = itemN + '\t'

  hr, miN, sec = line[ 1 ].split(':')
  itemN = itemN + hr + '\t' + miN + '\t' + sec + '\t'
  itemN = itemN + line[ len(line)-1 ]

  aList.append( itemN )
  itemN = ""

aList = list ( set ( aList ) )
aList1 = list ( set ( aList1 ) )

#for line in aList1:
#  print line

########################## ZIP FILE ##########################

import time
t=time.localtime()
mon = str(t.tm_mon)
if t.tm_mon < 10:
  mon = '0' + mon
day = str(t.tm_mday)
if t.tm_mday < 10:
  day = '0' + day
hour = str(t.tm_hour)
if t.tm_hour < 10:
  hour = '0' + hour
min1 = str(t.tm_min)
if t.tm_min < 10:
  min1 = '0' + min1
sec1 = str(t.tm_sec)
if t.tm_sec < 10:
  sec1 = '0' + sec1

time1 = str(t.tm_year)+' '+mon+' '+day+' '+hour+' '+min1+' '+sec1

fname1 = '/home/nflcwebdev/log_processing/' + str(t.tm_year)+mon+day + '_mysql_err.log'
print fname1
fname11 = '/home/nflcwebdev/log_processing/' + str(t.tm_year)+mon+day + '_mysql_err_info.log'
print fname11
print time1

import os

os.system('ls -l ' + fname1)
os.system('ls -l ' + fname11)
itemList = aList
itemList1 = aList1
if not os.path.exists( fname1 ):
  print fname1 + " NOT EXIST\n"
  outfile = open( fname1, 'w')
  for line in itemList:
    if line[-1]!='\n':
      line = line + '\n'
    outfile.write( line )
  outfile.close()
else:
  print fname1 + " IS HERE\n"
  infile = open( fname1, 'r')
  for line in infile:
    line = line.strip(' \n\r')
    itemList.append( line )

  itemList = list( set( itemList ) )

  outfile = open( fname1, 'w')
  for line in itemList:
    if line[-1]!='\n':
      line = line + '\n'
    outfile.write( line )
  outfile.close()

if not os.path.exists( fname11 ):
  print fname11 + " NOT EXIST\n"
  outfile1 = open( fname11, 'w')
  for line in itemList1:
    if line[-1]!='\n':
      line = line + '\n'
    outfile1.write( line )
  outfile1.close()
else:
  print fname11 + " IS HERE\n"
  infile1 = open( fname11, 'r')
  for line in infile1:
    line = line.strip(' \n\r')
    itemList1.append( line )

  itemList1 = list( set( itemList1 ) )

  outfile1 = open( fname11, 'w')
  for line in itemList1:
    if line[-1]!='\n':
      line = line + '\n'
    outfile1.write( line )
  outfile1.close()

# 59 * * * * /usr/bin/python /home/nflcwebdev/log_processing/mysql_error.py >>/home/nflcwebdev/log_processing/cron_mysql_error.log 2>&1
if not (t.tm_hour == 23 and t.tm_min == 59) :
  sys.exit('NOT ZIP ' + fname1 + ' ' + fname11 +' YET, EXIT')
##################################### SQLITE ###########################

import sqlite3
if not os.path.exists( '/home/nflcwebdev/log_processing/test_mysql_error.db' ):
  os.system('touch /home/nflcwebdev/log_processing/test_mysql_error.db/')
  conn = sqlite3.connect('/home/nflcwebdev/log_processing/test_mysql_error.db')
  c = conn.cursor()
  c.execute('CREATE TABLE mysql_error_log(YEAR INT NOT NULL,MONTH INT NOT NULL,DAY INT NOT NULL,HOUR INT NOT NULL,MINUTE INT NOT NULL,SECOND INT NOT NULL,ERROR VARCHAR(255))')
  c.execute('CREATE TABLE mysql_error_info_log(INFO VARCHAR(255))')
  conn.commit()
else:
  conn = sqlite3.connect('/home/nflcwebdev/log_processing/test_mysql_error.db')
  c = conn.cursor()
  c.execute('DROP TABLE mysql_error_info_log')
  c.execute('CREATE TABLE mysql_error_info_log(INFO VARCHAR(255))')
  c.execute('DROP TABLE mysql_error_log')
  c.execute('CREATE TABLE mysql_error_log(YEAR INT NOT NULL,MONTH INT NOT NULL,DAY INT NOT NULL,HOUR INT NOT NULL,MINUTE INT NOT NULL,SECOND INT NOT NULL,ERROR VARCHAR(255))')
  c = conn.cursor()

for line in itemList:
  line = line.split('\t')
  c.execute('INSERT INTO mysql_error_log VALUES(?,?,?,?,?,?,?)',line)
for line in itemList1:
  line = [ line ]
  c.execute('INSERT INTO mysql_error_info_log VALUES(?)',line)
conn.commit()
for row in c.execute('SELECT * FROM mysql_error_log;'):
  print row
for row in c.execute('SELECT count(*) FROM mysql_error_log;'):
  print row

for row in c.execute('SELECT * FROM mysql_error_info_log;'):
  print row
for row in c.execute('SELECT count(*) FROM mysql_error_info_log;'):
  print row
conn.close()
print len(itemList), len(itemList1)
os.system( 'ls -l ' + fname1 )
os.system( 'ls -l ' + fname11 )
os.system( 'gzip -f ' + fname1 )
os.system( 'gzip -f ' + fname11 )
os.system( 'ls -l ' + fname1 +'.gz' )
os.system( 'ls -l ' + fname11 +'.gz' )
os.system( 'rm -f ' + fname1 )
os.system( 'rm -f ' + fname11 )

print fname1
print fname11
print time1
os.system( 'date' )

